# git
ELLIOT
Quelques commandes utiles

| Objectif | Commande | Description/Comments |
| ------ | ------ | ------ |
| Renseigner les informations d'utilisateur | 	git config --global user.name "Mon nom" <br /> git config --global user.email utilisateur@mail.com | Renseigner le nom d'utilisateur l'adresse mail qui seront utilisés pour les commits. |
| Créer un dépôt local | git init | Créer un dépôt dans un répertoire local | 

To do : 

- git clone username@host:/path/to/repository
- git add <filename>
- git add *
- git commit -m "Commit message"
- git commit -a
- git push origin master
- git status
- git remote add origin <server>
- git remote -v
- git checkout -b <branchname>
- git checkout <branchname>
- git branch
- git branch -d <branchname>
- git push origin <branchname>
- git push --all origin
- git push origin :<branchname>
- git merge <branchname>
- git diff
- git diff --base <filename>
- git diff <sourcebranch> <targetbranch>
- git add <filename>
- git tag 1.0.0 <commitID>
- git log
- git push --tags origin
- git checkout -- <filename>
- git fetch origin
- git reset --hard origin/master